{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Plugin.WestonSurfaceTexture
  ( GodotWestonSurfaceTexture(..)
  , newWestonSurfaceTexture
  , updateTexture
  ) where

import Simula.WaylandServer
import Simula.Weston

import Control.Monad
import Data.Coerce

import           Plugin.Imports

import qualified Godot.Methods               as G
import qualified Godot.Gdnative.Internal.Api as Api

import qualified Godot.Core.GodotImage       as Image

import           Godot.Extra.Register

import Foreign


data GodotWestonSurfaceTexture = GodotWestonSurfaceTexture
  { _gwstObj          :: GodotObject
  , _gwstImage        :: GodotImage
  , _gwstSurface      :: TVar WestonSurface
  , _gwstView         :: TVar WestonView
  }

instance GodotClass GodotWestonSurfaceTexture where
  godotClassName = "WestonSurfaceTexture"

instance ClassExport GodotWestonSurfaceTexture where
  classInit obj  = GodotWestonSurfaceTexture obj
    <$> unsafeInstance GodotImage "Image"
    <*> newTVarIO (error "No WestonSurface initialized.")
    <*> newTVarIO (error "No WestonView initialized.")

  classExtends = "ImageTexture"
  classMethods = []

instance HasBaseClass GodotWestonSurfaceTexture where
  type BaseClass GodotWestonSurfaceTexture = GodotImageTexture
  super = GodotImageTexture . _gwstObj


-- | GodotWestonSurfaceTexture smart constructor
newWestonSurfaceTexture :: WestonSurface -> WestonView -> IO GodotWestonSurfaceTexture
newWestonSurfaceTexture ws view = do
  gwst <- unsafeNewNS [] "res://addons/godot-wayland/WestonSurfaceTexture.gdns"
    >>= fromNativeScript
  atomically $ do
    writeTVar (_gwstSurface gwst) ws
    writeTVar (_gwstView gwst) view
  return gwst


-- | Updates the ImageTexture using the WestonSurface
updateTexture :: GodotWestonSurfaceTexture -> IO GodotWestonSurfaceTexture
updateTexture gwst = do
  ws <- readTVarIO $ _gwstSurface gwst
  updateImageTexture ws (safeCast gwst)


updateImageTexture :: WestonSurface -> GodotWestonSurfaceTexture -> IO GodotWestonSurfaceTexture
updateImageTexture ws gwst =
  (_gwstImage gwst)
    & updateImage ws
    >>= createFromImage
 where
  createFromImage img = do
    G.create_from_image gwst img (7 .|. 16 :: Int)
    return gwst


-- | Update the image content with the WestonSurface
updateImage :: WestonSurface -> GodotImage -> IO GodotImage
updateImage ws img = do
  buffer <- westonSurfaceBuffer ws
  when (coerce buffer /= nullPtr) $ do
    res <- westonBufferResource buffer
    shmbuf <- wl_shm_buffer_get res

    when (coerce shmbuf /= nullPtr) $ do
      dt <- wl_shm_buffer_get_data shmbuf
      width <- wl_shm_buffer_get_width shmbuf
      height <- wl_shm_buffer_get_height shmbuf
      stride <- wl_shm_buffer_get_stride shmbuf
      shmFmt <- wl_shm_buffer_get_format shmbuf
      let fmt = asGodotFormat shmFmt
      let size :: Integral a => a
          size = fromIntegral (stride * height)

      byteArray <- Api.godot_pool_byte_array_new
      cursize <- Api.godot_pool_byte_array_size byteArray
      when (cursize /= size) $ Api.godot_pool_byte_array_resize byteArray size

      writeAccess <- Api.godot_pool_byte_array_write byteArray
      writePtr <- Api.godot_pool_byte_array_write_access_ptr writeAccess

      allocaArray size $ \arrPtr -> do
        wl_shm_buffer_begin_access shmbuf
        copyBytes arrPtr (castPtr dt) size
        wl_shm_buffer_end_access shmbuf

        let arrPtr32 = castPtr arrPtr :: Ptr Word32
        forM_ [0..size `div` 4] $ \n -> do
          let ptr = advancePtr arrPtr32 n
          elem <- peek ptr
          -- HACKHACK
          let [b3,b2,b1,b0] = map (\n -> shiftR elem (n*8) .&. 0xff) [0..3]
          let result = foldr (\n res -> shiftL res 8 .|. n) 0 [b1,b2,b3,b0]

          poke ptr result

        copyBytes writePtr arrPtr size
      Api.godot_pool_byte_array_write_access_destroy writeAccess -- TODO helper function


      G.create_from_data img width height False fmt byteArray
      Api.godot_pool_byte_array_destroy byteArray
  return img
 where
  asGodotFormat shmFmt = case shmFmt of
    WlShmFormatArgb8888 -> Image.FORMAT_RGBA8
    _ -> error $ "Unknown SHM format " ++ show shmFmt


