{-# OPTIONS_GHC -fno-warn-incomplete-uni-patterns #-}
{-# LANGUAGE TypeFamilies          #-}
module Plugin.Weston
  ( GodotWestonCompositor(..)
  ) where

import Simula.WaylandServer
import Simula.Weston
import Simula.WestonDesktop

import           Plugin.Imports
import           Plugin.Input
import           Plugin.WestonSurfaceTexture
import           Plugin.WestonSurfaceSprite

import           Godot.Nativescript
import qualified Godot.Methods               as G
import           Godot.Extra.Register

import qualified Data.Map.Strict             as M
import           Control.Monad
import           Control.Monad.Extra              (whenJust, whenJustM)
import           Control.Concurrent               (forkOS, threadDelay)
import           System.Environment               (getEnv, setEnv)


import           Foreign                          (nullPtr, castPtr,
                                                   with, sizeOf)


data GodotWestonCompositor = GodotWestonCompositor
  { _gwcObj         :: GodotObject
  , _gwcCompositor  :: TVar WestonCompositor
  , _gwcWlDisplay   :: TVar WlDisplay
  , _gwcSurfaces    :: TVar (M.Map WestonSurface GodotWestonSurfaceTexture)
  , _gwcUseSprites  :: TVar Bool
  , _gwcSprites     :: TVar (M.Map WestonSurface GodotWestonSurfaceSprite)
  , _gwcOutput      :: TVar WestonOutput
  , _gwcNormalLayer :: TVar WestonLayer
  }

instance GodotClass GodotWestonCompositor where
  godotClassName = "WestonCompositor"

instance ClassExport GodotWestonCompositor where
  classInit obj  = GodotWestonCompositor obj
    <$> newTVarIO undefined
    <*> newTVarIO undefined
    <*> newTVarIO mempty
    <*> newTVarIO False
    <*> newTVarIO mempty
    <*> newTVarIO undefined
    <*> newTVarIO undefined

  classExtends = "Spatial"
  classMethods =
    [ func NoRPC "_ready" [] $
        \self [] ->
          startBaseThread self

    , func NoRPC "_unhandled_input" ["InputEvent"] $
        \self [evObj] ->
          (fromGodotVariant evObj :: IO GodotObject)
            >>= asClass GodotInputEventKey "InputEventKey"
            >>= flip whenJust (unhandledInput self)

    , func NoRPC "get_surface_textures" [] $
        \self [] -> do
          surfaces <- M.elems <$> readTVarIO (_gwcSurfaces self)
          mapM (toLowLevel . toVariant . asObj) surfaces
            >>= toLowLevel
            :: IO GodotArray

    , func NoRPC "create_window_sprites" [] $
        \self [] ->
          createWindowSprites self

    , func NoRPC "use_sprites" ["bool"] $
        \self [vtIsUse] -> do
          isUse <- fromGodotVariant vtIsUse
          -- Also remove spawned sprites if not isUse?
          when isUse $ createWindowSprites self
          atomically $ writeTVar (_gwcUseSprites self) isUse

    , func NoRPC "set_focus" ["WestonSurfaceSprite", "bool"] $
        \self [vtWST, vtIsFocus] -> do
          wst <- fromNativeScript =<< fromGodotVariant vtWST
          setFocus self wst =<< fromGodotVariant vtIsFocus

    ] {-# OPTIONS_GHC -fwarn-incomplete-uni-patterns #-}

instance HasBaseClass GodotWestonCompositor where
  type BaseClass GodotWestonCompositor = GodotSpatial
  super = GodotSpatial . _gwcObj


unhandledInput :: GodotWestonCompositor -> GodotInputEventKey -> IO ()
unhandledInput compositor evk = do
  dsp <- readTVarIO (_gwcWlDisplay compositor)
  kbd <- getKeyboard compositor
  processKeyEvent dsp kbd evk
  setInputHandled compositor
 where
  getKeyboard :: GodotWestonCompositor -> IO WestonKeyboard
  getKeyboard gwc = getSeat gwc >>= weston_seat_get_keyboard


createWindowSprites :: GodotWestonCompositor -> IO ()
createWindowSprites compositor = do
  (surfaces, sprites) <- atomically $ do
    surfaces <- readTVar (_gwcSurfaces compositor)
    sprites  <- readTVar (_gwcSprites compositor)
    return (surfaces, sprites)

  let nonSpawned = M.elems $ M.difference surfaces sprites
  forM_ nonSpawned $ \wst ->
    createSprite wst compositor


setFocus :: GodotWestonCompositor -> GodotWestonSurfaceTexture -> Bool -> IO ()
setFocus compositor wst = \case
  True -> do
    seat    <- getSeat compositor
    kbd     <- weston_seat_get_keyboard seat
    pointer <- weston_seat_get_pointer seat

    (view, ws) <- atomically $ do
      view <- readTVar (_gwstView wst)
      ws   <- readTVar (_gwstSurface wst)
      return (view, ws)

    weston_pointer_set_focus pointer view 0 0
    weston_keyboard_set_focus kbd ws

  False -> undefined -- TODO: Implement unsetting focus
    {-
     -seat    <- getSeat compositor
     -kbd     <- weston_seat_get_keyboard seat
     -pointer <- weston_seat_get_pointer seat
     -
     -weston_pointer_clear_focus pointer
     -weston_keybeard_clear_focus kbd
     -}


createSprite :: GodotWestonSurfaceTexture -> GodotWestonCompositor -> IO GodotWestonSurfaceSprite
createSprite wst compositor = do
  sprite <- newGodotWestonSurfaceSprite wst =<< getSeat compositor
  G.add_child compositor (asObj sprite) True
  ws <- readTVarIO (_gwstSurface wst)
  atomically $ modifyTVar' (_gwcSprites compositor) $ M.insert ws sprite
  return sprite


startBaseThread :: GodotWestonCompositor -> IO ()
startBaseThread compositor = void $ forkOS $ do
  prevDisplay <- getEnv "DISPLAY"

  wldp  <- wl_display_create
  wcomp <- weston_compositor_create wldp nullPtr
  atomically $ writeTVar (_gwcCompositor compositor) wcomp
  atomically $ writeTVar (_gwcWlDisplay compositor) wldp
  westonCompositorSetRepaintMsec wcomp 1000

  setup_weston_log_handler
  westonCompositorSetEmptyRuleNames wcomp

  --todo hack; make this into a proper withXXX function
  res <- with (WestonHeadlessBackendConfig (WestonBackendConfig westonHeadlessBackendConfigVersion (sizeOf (undefined :: WestonHeadlessBackendConfig)))
           False) $ weston_compositor_load_backend wcomp WestonBackendHeadless . castPtr

  when (res > 0) $ ioError $ userError "Error when loading backend"

  socketName <- wl_display_add_socket_auto wldp
  putStrLn $ "Socket: " ++ socketName
  setEnv "WAYLAND_DISPLAY" socketName

  mainLayer <- newWestonLayer wcomp
  weston_layer_set_position mainLayer WestonLayerPositionNormal

  atomically $ writeTVar (_gwcNormalLayer compositor) mainLayer


  windowedApi <- weston_windowed_output_get_api wcomp

  let outputPendingSignal = westonCompositorOutputPendingSignal wcomp
  outputPendingPtr <- createNotifyFuncPtr (onOutputPending windowedApi)
  addListenerToSignal outputPendingSignal outputPendingPtr

  let outputCreatedSignal = westonCompositorOutputCreatedSignal wcomp
  outputCreatedPtr <- createNotifyFuncPtr onOutputCreated
  addListenerToSignal outputCreatedSignal outputCreatedPtr

  --createFlushDamageFunc (onFlushDamage compositor) >>= setFlushDamageFunc wcomp

  westonWindowedOutputCreate windowedApi wcomp "Godot"

  output <- atomically $ readTVar (_gwcOutput compositor)

  forkOS $ forever $ weston_output_schedule_repaint output >> threadDelay 1000

  let api = defaultWestonDesktopApi
        { apiSurfaceAdded   = onSurfaceCreated
        , apiSurfaceRemoved = onSurfaceDestroyed
        , apiCommitted      = onSurfaceCommit
        }


  westonDesktopCreate wcomp api nullPtr

  seat <- newSeat wcomp "Godot"
  weston_seat_init_pointer seat
  weston_seat_init_keyboard seat (XkbKeymap nullPtr)

  --installHandler sigUSR1 Ignore Nothing
  wet_load_xwayland wcomp

  -- Needs to be set to the original X display rather than
  -- the new one for some reason, or it will crash.
  setEnv "DISPLAY" prevDisplay

  weston_compositor_wake wcomp
  putStrLn "starting compositor"

  wl_display_run wldp

  where
    onOutputPending windowedApi _ outputPtr = do
      let output = WestonOutput $ castPtr outputPtr
      weston_output_set_scale output 1
      weston_output_set_transform output 0
      westonWindowedOutputSetSize windowedApi output 1280 720
      weston_output_enable output
      return ()

    onOutputCreated _ outputPtr = do
      let output = WestonOutput $ castPtr outputPtr
      atomically $ writeTVar (_gwcOutput compositor) output

    onSurfaceCreated desktopSurface  _ = do
      surface <- weston_desktop_surface_get_surface desktopSurface
      view'   <- weston_desktop_surface_create_view desktopSurface
      output  <- atomically $ readTVar (_gwcOutput compositor)
      layer   <- atomically $ readTVar (_gwcNormalLayer compositor)
      westonViewSetOutput view' output
      weston_layer_entry_insert (westonLayerViewList layer) (westonViewLayerEntry view')

      wsTex   <- newWestonSurfaceTexture surface view'
      atomically $ modifyTVar' (_gwcSurfaces compositor) (M.insert surface wsTex)

      whenM (readTVarIO (_gwcUseSprites compositor)) $
        void $ createSprite wsTex compositor

    onSurfaceDestroyed desktopSurface _ = do
      surface <- weston_desktop_surface_get_surface desktopSurface
      atomically $ modifyTVar' (_gwcSurfaces compositor) (M.delete surface)

    onSurfaceCommit desktopSurface _ _ _ = do
      surface     <- weston_desktop_surface_get_surface desktopSurface

      readTVarIO (_gwcSprites compositor)
        <&> M.lookup surface >>= \case
          Just sprite -> void $ updateWestonSurfaceSprite sprite
          Nothing     ->
            whenJustM (M.lookup surface <$> readTVarIO (_gwcSurfaces compositor)) $
              void . updateTexture


getSeat :: GodotWestonCompositor -> IO WestonSeat
getSeat gwc = do
  atomically (readTVar (_gwcCompositor gwc)) >>= westonCompositorSeats
    >>= \case
      seat:_ -> return seat
      []     -> error "No seats :("

