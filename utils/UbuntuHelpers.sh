installUbuntuDependencies() {
    ask "Install system dependencies? (requires sudo)" Y \
        && sudo apt install -y   \
                libpixman-1-dev  \
                libweston-3-dev  \
                libegl1-mesa-dev \
                weston           \
                curl             \

                # Godot:
                libasound2    \
                libasyncns0   \
                libbsd0       \
                libdbus-1-3   \
                libflac8      \
                libfreetype6  \
                libgpg-error0 \
                liblz4-1
                liblzma5      \
                libogg0       \
                libpng16-16   \
                libpulse0     \
                libsndfile1   \
                libvorbis0a   \
                libvorbisenc2 \
                libwrap0      \
                libxau6       \
                libxcb1       \
                libxdmcp6     \
                libxext6      \
                libxfixes3    \
                libxinerama1  \
                libxrandr2    \
                libxrender1   \
                zlib1g        \
}

# It is best to install stack via curl to avoid
# old versions found in Ubuntu's package stores.
installStack() {
    if [ -z `which stack` ]; then
        echo "Installing Stack"
        curl -sSL https://get.haskellstack.org/ | sh -s - -f
    else echo "Stack already installed"
    fi
}

# Use this if you already have stack installed via apt-get install.
upgradeStack() {
  stack upgrade
  echo ""
  # Same with old versions of hpack
  echo "Installing hpack"
  stack install hpack
}
