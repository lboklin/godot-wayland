all:
	cd addons/godot-wayland && make ; cd -
run:
	cd addons/godot-wayland && make run ; cd -
watch:
	cd addons/godot-wayland && make watch ; cd -
install:
	cd addons/godot-wayland && make install TARGET=$(TARGET) ; cd -
